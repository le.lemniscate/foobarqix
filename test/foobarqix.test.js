import foobarqixImperative from '../src/foobarqix_imperative';
import foobarqixMapReduce from '../src/foobarqix_mapreduce';
import foobarqixObject from '../src/foobarqix_object';

const strategies = {
  'imperative programming': foobarqixImperative,
  'mapreduce style': foobarqixMapReduce,
  'object oriented': foobarqixObject,
};

Object.entries(strategies).forEach(([paradigm, foobarqix]) => {
  describe(`Foobarqix - ${paradigm}`, () => {
    it('returns foo when 3 or 6 is given as input', () => {
      expect(foobarqix(3)).toEqual('foo');
      expect(foobarqix(6)).toEqual('foo');
    });
    it('returns bar when 5 or 10 is given as input', () => {
      expect(foobarqix(5)).toEqual('bar');
      expect(foobarqix(10)).toEqual('bar');
    });
    it('returns qix when 7 or 14 is given as input', () => {
      expect(foobarqix(7)).toEqual('qix');
      expect(foobarqix(14)).toEqual('qix');
    });
    it('returns the value when 1, 2, 4, 8 is given as input', () => {
      expect(foobarqix(1)).toEqual(1);
      expect(foobarqix(2)).toEqual(2);
      expect(foobarqix(4)).toEqual(4);
      expect(foobarqix(8)).toEqual(8);
    });
    it('returns foobar when 15 is given as input', () => {
      expect(foobarqix(15)).toEqual('foobar');
    });
    it('returns barqix when 35 is given as input', () => {
      expect(foobarqix(35)).toEqual('barqix');
    });
    it('returns foobarqix when 105 is given as input', () => {
      expect(foobarqix(105)).toEqual('foobarqix');
    });
  });
});
