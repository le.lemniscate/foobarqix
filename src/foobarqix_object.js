import NumericValue from './objects/NumericValue';

const computes = [
  (currentValue, input) => (input % 3 === 0 ? currentValue.foo() : currentValue),
  (currentValue, input) => (input % 5 === 0 ? currentValue.bar() : currentValue),
  (currentValue, input) => (input % 7 === 0 ? currentValue.qix() : currentValue),
];

export default (value) => computes
  .reduce((acc, compute) => compute(acc, value), new NumericValue(value))
  .repr();
