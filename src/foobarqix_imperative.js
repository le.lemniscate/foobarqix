export default (value) => {
  let result = '';
  if (value % 3 === 0) {
    result += 'foo';
  }
  if (value % 5 === 0) {
    result += 'bar';
  }
  if (value % 7 === 0) {
    result += 'qix';
  }
  return result.length === 0 ? value : result;
};
