const computes = [
  (value) => (value % 3 === 0 ? 'foo' : undefined),
  (value) => (value % 5 === 0 ? 'bar' : undefined),
  (value) => (value % 7 === 0 ? 'qix' : undefined),
];

export default (value) => computes
  .map((compute) => compute(value))
  .filter((result) => result !== undefined)
  .reduce((acc, result) => (Number.isInteger(acc) ? result : acc + result), value);
