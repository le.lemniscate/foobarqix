import Void from './Void';

export default class LiteralValue {
  constructor(value, content = new Void()) {
    this.value = value;
    this.content = content;
  }

  bar() {
    return new LiteralValue('bar', this);
  }

  qix() {
    return new LiteralValue('qix', this);
  }

  repr() {
    return `${this.content.repr()}${this.value}`;
  }
}
