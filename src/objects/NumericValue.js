import LiteralValue from './LiteralValue';

export default class NumericValue {
  constructor(content) {
    this.content = content;
  }

  foo() {
    return new LiteralValue('foo');
  }

  bar() {
    return new LiteralValue('bar');
  }

  qix() {
    return new LiteralValue('qix');
  }

  repr() {
    return this.content;
  }
}
